# Intro

A simple class to set reduce output filename based on a field of the key.


# Compile

$ ./gradlew build

the jar  build/libs/streaming-multiple-output-0.1.jar is ready to be used.

# Use it
Parameters to hadoop-streaming (for hadoop > 2.0) 

-libjars <path to streaming-multiple-output-0.1.jar> 
    (it must be the first parameter apparently)

-outputformat com.amadeus.ssp.pam.SplitOnKeyContent

By default it assumes the key is a "^" separated list of fields, and it takes
the first one, i.e. the number 0, to prefix outputfiles.

If this is not the case, use the options

-D map.output.key.field.separa=<separator>
-D map.output.key.filename=<index>

The first option is the same used by org.apache.hadoop.mapred.lib.FieldSelectionMapReduce.

