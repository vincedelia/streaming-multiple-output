package com.amadeus.ssp.pam;

 import org.apache.hadoop.fs.Path;
 import org.apache.hadoop.fs.FileSystem;
 import org.apache.hadoop.mapred.JobConf;
 import org.apache.hadoop.mapred.RecordWriter;
 import org.apache.hadoop.io.Text;
 import org.apache.hadoop.mapred.lib.MultipleTextOutputFormat;
 import java.util.regex.Pattern;
 import java.util.Arrays;
 import org.apache.hadoop.util.Progressable;
 import java.io.IOException;
 import org.apache.commons.lang.StringUtils;

 public class SplitOnKeyContent extends MultipleTextOutputFormat<Text, Text> {

       public String separator="\\^";
       public int index=0;
       public Boolean strip = false;

       protected RecordWriter<Text,Text> getBaseRecordWriter(
               FileSystem fs,
               JobConf job,
               String name,
               Progressable arg3) 
           throws IOException {

           String rawSeparator = job.get("map.output.key.field.separa");
           if(rawSeparator == null) {
               rawSeparator = "^";
           }
           separator = Pattern.quote(rawSeparator);
           index = job.getInt("map.output.key.filename", 0);
           strip = job.getBoolean("map.output.key.strip", false);
           return super.getBaseRecordWriter(fs, job, name, arg3);
       }
        /**
        * Use they key as part of the path for the final output file.
        */
       @Override
       protected String generateFileNameForKeyValue(Text key, Text value, String leaf) {
           String prefix = key.toString();
           String[] tokens = prefix.split(this.separator);
           prefix = prefix.substring(0, 1);

           if(tokens.length > this.index) {
               prefix = tokens[this.index];
           }
           return new Path(prefix, leaf).toString();
       }


       /*
        * If strip is True, strip the prefix from the key before writing
        */
       @Override
       protected Text generateActualKey(Text key, Text value) {
           String strippedKey = key.toString();
           String[] tokens = strippedKey.split(this.separator);
           if (strip) {
               strippedKey = StringUtils.join( Arrays.copyOfRange( tokens, 1, tokens.length ), this.separator);
           }
           Text sKey = new Text(strippedKey);
           return sKey;
       }

 }
